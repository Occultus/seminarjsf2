import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class SimpleBufferTest {
    @Test
    public void ComplexBufferTest() throws BufferException {
        SimpleBuffer<Integer> buffer = new SimpleBuffer<>(3);
        buffer.add(14);
        buffer.add(20);
        buffer.add(3);

        assertThrows(BufferException.class, () -> buffer.add(22));
        assertEquals(buffer.getLen(), 3);
        assertEquals(buffer.pop(), 14);
        buffer.clear();
        assertEquals(0, buffer.getLen());
        assertThrows(BufferException.class, buffer::pop);
    }
}
