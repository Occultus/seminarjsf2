import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleTaskProcessorTest {
    @Test
    public void ComplexSimpleProcessorGenerator() throws BufferException, ProcessorException {
        assertThrows(ProcessorException.class, ()->new SimpleTaskProcessor(null));

        SimpleBuffer<Task> buffer = new SimpleBuffer<>();
        buffer.add(new Task(21, 3, 4));
        buffer.add(new Task(32, 4, 123));
        buffer.add(new Task(5, 23, 111));

        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);
        assertEquals(28, processor.process());
        assertEquals(77, processor.process(new Task(11, 22, 44)));

    }
}
