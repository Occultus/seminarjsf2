import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SimpleTaskGeneratorTest {
    @Test
    public void ComplexSimpleTaskGeneratorTest() throws GeneratorException, BufferException {
        assertThrows(GeneratorException.class, ()->new SimpleTaskGenerator(null, 12, 2));
        assertThrows(GeneratorException.class, ()->new SimpleTaskGenerator(null, 12, -2));

        SimpleTaskGenerator generator = new SimpleTaskGenerator(new SimpleBuffer<>(), 41, 3);
        generator.generate();

        Task task = new Task(41,42,43);
        assertEquals(task, generator.getBuffer().pop());

        generator.withStartValue(9).withAmount(2).generate();
        Task task1 = new Task(9, 10);
        generator.generate();
        assertEquals(task1, generator.getBuffer().pop());

    }


}
