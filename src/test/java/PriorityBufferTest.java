import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PriorityBufferTest {
    @Test
    public void ComplexPriorityBufferTest() throws BufferException {
        PriorityTaskGenerator generator = new PriorityTaskGenerator();
        generator.withStartValue(-12).withAmount(4).withPriority(2).generate();
        PriorityTask task = new PriorityTask(2, -12, -11, -10, -9);
        assertEquals(task, generator.getBuffer().pop());
    }
}
