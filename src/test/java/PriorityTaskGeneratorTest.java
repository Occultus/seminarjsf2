import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PriorityTaskGeneratorTest {
    @Test
    public void ComplexPriorityBufferTest() throws BufferException {
        PriorityBuffer buffer = new PriorityBuffer();
        buffer.add(new PriorityTask(5, 2, 4 ,1));
        buffer.add(new PriorityTask(1, 5, 3 ,2));
        buffer.add(new PriorityTask(10, 3));
        buffer.add(new PriorityTask(-2, 2, 3));

        PriorityTask task = new PriorityTask(10, 3);
        PriorityTask task1 = new PriorityTask(5, 2, 4, 1);
        assertEquals(task, buffer.pop());
        assertEquals(task1, buffer.pop());
        assertEquals(2, buffer.getLen());
        buffer.clear();
        assertEquals(0, buffer.getLen());
        assertThrows(BufferException.class, buffer::pop);
    }
}
