public class SimpleTaskProcessor implements ITaskProcessor {
    private final IBuffer<Task> buffer;

    public SimpleTaskProcessor(){
        buffer = new SimpleBuffer<>();
    }

    public SimpleTaskProcessor(IBuffer<Task> buffer) throws ProcessorException {
        if(buffer == null){
            throw new ProcessorException("Buffer is Null pointer");
        }
        this.buffer = buffer;
    }

    @Override
    public Integer process() throws BufferException {
        if(buffer.getLen() == 0){
            return null;
        }
        int res = 0;
        for(int value: buffer.pop().getData()){
            res += value;
        }
        return res;
    }

    @Override
    public Integer process(Task task) {
        if(task == null){
            return null;
        }
        int res = 0;
        for(int value: task.getData()){
            res += value;
        }
        return res;
    }

}
