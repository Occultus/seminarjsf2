public class GeneratorException extends Exception {
    public GeneratorException() {
        super();
    }

    public GeneratorException(String errorText) {
        super(errorText);
    }

    public GeneratorException(Exception exception) {
        super(exception);
    }

    public GeneratorException(String errorText, Exception exception) {
        super(errorText, exception);
    }
}

