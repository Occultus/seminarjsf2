import java.util.Arrays;

public class Task implements ITask {
    private final int[] dataArr;

    public Task(){
        dataArr = new int[1];
    }

    public Task(int ... args){
        dataArr = args;
    }

    @Override
    public int[] getData() {
        return dataArr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(dataArr, task.dataArr);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(dataArr);
    }
}
