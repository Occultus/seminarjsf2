public interface IBuffer<T> {
    void add(T elem) throws BufferException;
    T pop() throws BufferException;
    int getLen();
    void clear();
}
