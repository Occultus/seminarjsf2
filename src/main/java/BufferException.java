public class BufferException extends Exception {
    public BufferException(){
        super();
    }
    public BufferException(String errorText){
        super(errorText);
    }
    public BufferException(Exception exception){
        super(exception);
    }
    public BufferException(String errorText, Exception exception){
        super(errorText,exception);
    }
}
