public class SimpleTaskGenerator implements ITaskGenerator{
    private final IBuffer<Task> buffer;
    private int startValue;
    private int amount;

    public SimpleTaskGenerator(){
        buffer = new SimpleBuffer<>();
        startValue = 0;
        amount = 0;
    }
    public SimpleTaskGenerator(IBuffer<Task> buffer, int startValue, int amount) throws GeneratorException {
        if(buffer == null){
            throw new GeneratorException("Buffer is Null pointer");
        }
        else if(amount < 0){
            throw new GeneratorException("Incorrect amount");
        }
        else{
            this.buffer = buffer;
            this.startValue = startValue;
            this.amount = amount;
        }
    }

    public IBuffer<Task> getBuffer() {
        return buffer;
    }

    @Override
    public void generate() throws BufferException {
        int [] taskArr = new int[amount];
        for(int i = 0; i < amount; i++){
            taskArr[i] = startValue + i;
        }
        buffer.add(new Task(taskArr));
    }

    public SimpleTaskGenerator withStartValue(int startValue){
        this.startValue = startValue;
        return this;
    }

    public SimpleTaskGenerator withAmount(int amount) throws GeneratorException {
        if(amount <= 0){
            throw new GeneratorException("Incorrect amount");
        }
        this.amount = amount;
        return this;
    }

}
