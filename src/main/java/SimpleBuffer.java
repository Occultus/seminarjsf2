import java.util.ArrayDeque;
import java.util.Objects;

public class SimpleBuffer<T> implements IBuffer<T>{
    private final ArrayDeque<T> buffer;
    private final int maxLen;

    public SimpleBuffer(int maxLen) throws BufferException {
        if(maxLen < 1){
            throw new BufferException("Incorrect max len of Buffer");
        }
        this.maxLen = maxLen;
        buffer = new ArrayDeque<>();
    }

    public SimpleBuffer(){
        maxLen = Integer.MAX_VALUE;
        buffer = new ArrayDeque<>();
    }

    @Override
    public void add(T elem) throws BufferException{
        if(getLen() >= maxLen){
            throw new BufferException("Impossible to add elem to buffer");
        }
        buffer.add(elem);
    }

    @Override
    public T pop() throws BufferException {
        if(buffer.isEmpty()){
            throw new BufferException("Empty buffer!");
        }
        return buffer.pop();
    }

    @Override
    public int getLen() {
        return buffer.size();
    }

    @Override
    public void clear() {
        buffer.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleBuffer<?> that = (SimpleBuffer<?>) o;
        return maxLen == that.maxLen &&
                Objects.equals(buffer, that.buffer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buffer, maxLen);
    }

}
