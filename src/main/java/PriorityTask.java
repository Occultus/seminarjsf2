import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public class PriorityTask implements ITask, Comparable<PriorityTask>{
    private final int[] dataArr;
    private final int priority;

    public PriorityTask(int priority, int... args){
        this.priority = priority;
        dataArr = args;
    }

    @Override
    public int[] getData() {
        return dataArr;
    }

    @Override
    public int compareTo(@NotNull PriorityTask obj) {
        return obj.priority - priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriorityTask task = (PriorityTask) o;
        return priority == task.priority &&
                Arrays.equals(dataArr, task.dataArr);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(priority);
        result = 31 * result + Arrays.hashCode(dataArr);
        return result;
    }
}
