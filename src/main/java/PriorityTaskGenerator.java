public class PriorityTaskGenerator implements ITaskGenerator{
    private final IBuffer<PriorityTask> buffer;
    private int startValue;
    private int amount;
    private int priority;
    public PriorityTaskGenerator(){
        buffer = new SimpleBuffer<>();
        startValue = 0;
        amount = 0;
        priority = 0;
    }
    public PriorityTaskGenerator(IBuffer<PriorityTask> buffer, int startValue, int amount, int priority)
            throws BufferException {
        if(buffer == null){
            throw new BufferException("Buffer is Null pointer");
        }
        else if(amount < 0){
            throw new BufferException("Incorrect amount");
        }
        else{
            this.buffer = buffer;
            this.startValue = startValue;
            this.amount = amount;
            this.priority = priority;
        }
    }

    @Override
    public void generate() throws BufferException {
        int [] taskArr = new int[amount];
        for(int i = 0; i < amount; i++){
            taskArr[i] = startValue + i;
        }
        buffer.add(new PriorityTask(priority, taskArr));
    }

    public IBuffer<PriorityTask> getBuffer() {
        return buffer;
    }

    public PriorityTaskGenerator withStartValue(int startValue){
        this.startValue = startValue;
        return this;
    }

    public PriorityTaskGenerator withAmount(int amount) throws BufferException {
        if(amount <= 0){
            throw new BufferException("Incorrect amount");
        }
        this.amount = amount;
        return this;
    }

    public PriorityTaskGenerator withPriority(int priority){
        this.priority = priority;
        return this;
    }


}
