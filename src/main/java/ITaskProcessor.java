public interface ITaskProcessor {
    Integer process(Task task);
    Integer process() throws BufferException;
}
