import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityBuffer implements IBuffer<PriorityTask>{
    private final Queue<PriorityTask> buffer;
    public PriorityBuffer(){
        buffer = new PriorityQueue<>(PriorityTask::compareTo);
    }

    @Override
    public void add(PriorityTask task) throws BufferException {
        if(task == null){
            throw new BufferException("Task is Null pointer");
        }
        buffer.offer(task);
    }

    @Override
    public PriorityTask pop() throws BufferException {
        if(getLen() == 0){
            throw new BufferException("Empty buffer");
        }
        return buffer.poll();
    }

    @Override
    public int getLen() {
        return buffer.size();
    }

    @Override
    public void clear() {
        buffer.clear();
    }
}
