public class ProcessorException extends Exception {
    public ProcessorException(){
        super();
    }
    public ProcessorException(String errorText){
        super(errorText);
    }
    public ProcessorException(Exception exception){
        super(exception);
    }
    public ProcessorException(String errorText, Exception exception){
        super(errorText,exception);
    };
}