public interface ITaskGenerator {
    void generate() throws BufferException;
}
